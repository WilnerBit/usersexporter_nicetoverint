﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint.Util
{
    public class VisualizacaoExcel
    {
        public void GeraExcelDados(List<DicionarioOrdenado<string, string>> dados)
        {
            var enderecoNice = ConfigurationManager.AppSettings.Get("PathExcelVisualizacaoEntrada");

            using(StreamWriter sw = new StreamWriter(enderecoNice, false))
            {
                var colunas = dados[0].Keys.ToList();
                sw.WriteLine(String.Join(";", colunas));

                dados.RemoveAt(0);

                foreach (DicionarioOrdenado<string, string> item in dados)
                {
                    var valores = item.Values.ToList();
                    sw.WriteLine(String.Join(";", valores));
                }
            }

        }
    }
}
