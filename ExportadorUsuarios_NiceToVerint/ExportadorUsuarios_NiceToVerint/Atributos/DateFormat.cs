﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint.Atributos
{
    public class DateFormat : System.Attribute
    {
        //public string Formato { get; set; }

        //public DateFormat(string formato)
        //{
        //    this.Formato = formato;
        //}

        public string[] Formatos = new String[]
            { "MMddyyyy", "MM/d/yyyy", "M/dd/yyyy", "M/d/yyyy", "MM/dd/yyyy" };
    }
}
