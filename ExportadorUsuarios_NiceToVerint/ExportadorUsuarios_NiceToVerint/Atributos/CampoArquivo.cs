﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint.Atributos
{
    public class CampoArquivo : System.Attribute
    {
        public string Nome { get; set; }

        public CampoArquivo(string nome)
        {
            this.Nome = nome;
        }
    }
}
