﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint.Atributos
{
    public class CampoCopiar : System.Attribute
    {
        public string Nome { get; set; }

        public CampoCopiar(string nome)
        {
            this.Nome = nome;
        }
    }
}
