﻿using ExportadorUsuarios_NiceToVerint.Util;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint
{
    public class FTPAcessorNice
    {
        protected List<string> LinhasArquivoNice;
        public List<string> CarregaLinhasArquivoNice()
        {

            var nomeArquivoAtual = GetInputFileName();
            //Ler Linhas
            LinhasArquivoNice = new List<string>();

            var responseLerLinhas = GetResponseGenericaNice(WebRequestMethods.Ftp.DownloadFile, nomeArquivoAtual);
            Stream responseStreamLerLinhas = responseLerLinhas.GetResponseStream();
            StreamReader readerLerLinhas = new StreamReader(responseStreamLerLinhas);
            var conteudo = readerLerLinhas.ReadToEnd();
            if (!String.IsNullOrWhiteSpace(conteudo))
                LinhasArquivoNice.AddRange(conteudo.Split(new string[1] { "\n" },
                    StringSplitOptions.RemoveEmptyEntries));

            readerLerLinhas.Close();
            responseStreamLerLinhas.Close();
            responseLerLinhas.Close();

            return LinhasArquivoNice;
        }


        protected string GetInputFileName()
        {
            var responseAcharArquivo = GetResponseGenericaNice(WebRequestMethods.Ftp.ListDirectory);
            Stream responseStreamAcharArquivo = responseAcharArquivo.GetResponseStream();
            StreamReader readerAcharArquivo = new StreamReader(responseStreamAcharArquivo);

            string arquivoTxtInput = string.Empty;
            var conteudo = readerAcharArquivo.ReadToEnd();
            if (!String.IsNullOrWhiteSpace(conteudo))
            {
                var linhas = conteudo.Split(new string[1] { Environment.NewLine },
                    StringSplitOptions.RemoveEmptyEntries);
                arquivoTxtInput = linhas.FirstOrDefault(a => a.Trim().EndsWith(".txt"));
            }

            readerAcharArquivo.Close();
            responseStreamAcharArquivo.Close();
            responseAcharArquivo.Close();

            return arquivoTxtInput;
        }
        protected FtpWebResponse GetResponseGenericaNice(string method, string fileName = "")
        {
            var enderecoNice = ConfigurationManager.AppSettings.Get("EnderecoFTPNice");
            var usuarioNice = ConfigurationManager.AppSettings.Get("UsuarioFTPNice");
            var senhaNice = ConfigurationManager.AppSettings.Get("SenhaFTPNice");

            if (!String.IsNullOrWhiteSpace(fileName))
                enderecoNice = String.Format(@"{0}/{1}", enderecoNice, fileName);

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(enderecoNice);
            request.Method = method;
            //request.Method = WebRequestMethods.Ftp.ListDirectory;

            // This example assumes the FTP site uses anonymous logon.  
            request.Credentials = new NetworkCredential(usuarioNice, senhaNice);
            request.KeepAlive = false;
            request.UseBinary = true;
            request.UsePassive = true;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            return response;
        }

    }
}
