﻿using ExportadorUsuarios_NiceToVerint.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint.ClassesModelo
{
    public class UsuarioNice
    {
        [CampoArquivo("custID")]
        public int clienteId { get; set; }

        [CampoArquivo("muID")]
        public int muID { get; set; }

        [CampoArquivo("muName")]
        public string NomeMu { get; set; }

        [CampoArquivo("date"), DateFormat]
        public DateTime? DataExportacao { get; set; }

        [CampoArquivo("TZ")]
        public string Mu_TZ { get; set; }

        [CampoArquivo("tvID")]
        public int? WFM_ID { get; set; }

        [CampoArquivo("acdID1")]
        public int? AgenciaID1 { get; set; } //Revisar

        [CampoArquivo("acdID2")]
        public int? AgenciaID2 { get; set; } //Revisar

        [CampoArquivo("logonID1")]
        public string LoginId1 { get; set; }

        [CampoArquivo("logonID2")]
        public string LoginId2 { get; set; }

        [CampoArquivo("ssn")]
        public long NumeroSegurancaSocial { get; set; }

        [CampoArquivo("agentName")]
        public string Nome { get; set; }

        [CampoArquivo("modify")]
        public string TimeStampModificacao { get; set; }

        [CampoArquivo("LOGINSECUNDARIO"), CampoEspecifico]
        public string LoginSecundario { get; set; }

        [CampoArquivo("CH_SEMANAL"), CampoEspecifico]
        public string CH_SEMANAL { get; set; }

        [CampoArquivo("CONSULTOR_AUTONOMO"), CampoEspecifico]
        public bool ConsultorAutonomo { get; set; }

        [CampoArquivo("COORDENADOR"), CampoEspecifico]
        public string Coordenador { get; set; }

        [CampoArquivo("CPF"), CampoEspecifico]
        public string CPF { get; set; }

        [CampoArquivo("DAC"), CampoEspecifico]
        public string DAC { get; set; }

        [CampoArquivo("DATA AUSENCIA FIM"), CampoEspecifico, DateFormat]
        public DateTime? DataAusenciaFim { get; set; }

        [CampoArquivo("DATA AUSENCIA INICIO"), CampoEspecifico, DateFormat]
        public DateTime? DataAusenciaInicio { get; set; }

        [CampoArquivo("DATA FÉRIAS FUT"), CampoEspecifico, DateFormat]
        public DateTime? DataFeriasFut { get; set; }

        [CampoArquivo("DATAADMISSAO"), CampoEspecifico, DateFormat]
        public DateTime? DataAdmissao { get; set; }

        [CampoArquivo("DATADEMISSAO_RM"), CampoEspecifico, DateFormat]
        public DateTime? DataDemissao { get; set; }

        [CampoArquivo("DIRETORATENDIMENTO"), CampoEspecifico]
        public string DiretorAtendimento { get; set; }

        [CampoArquivo("FUNCAORM"), CampoEspecifico]
        public string FuncaoRM { get; set; }

        [CampoArquivo("GERENTE"), CampoEspecifico]
        public string Gerente { get; set; }

        [CampoArquivo("GERENTE_EXECUTIVO"), CampoEspecifico]
        public string GerenteExecutivo { get; set; }

        [CampoArquivo("HORARIOFIM_FORPONTO"), CampoEspecifico]
        public string HorarioFimPonto { get; set; }

        [CampoArquivo("ID_SEGMENTO_1"), CampoEspecifico]
        public int? IDSegmento1 { get; set; }

        [CampoArquivo("MATRICULA"), CampoEspecifico]
        public string Matricula { get; set; }

        [CampoArquivo("NOME"), CampoEspecifico]
        public string Nome_ { get; set; } //Verificar

        [CampoArquivo("OPERACAOHOMINUM"), CampoEspecifico]
        public string OperacaoHorasMinimas { get; set; }

        [CampoArquivo("PIN"), CampoEspecifico]
        public int? Pin { get; set; }

        [CampoArquivo("PLANEJAMENTO"), CampoEspecifico]
        public string PlanejamentoHorario { get; set; }

        [CampoArquivo("SEGMENTO1"), CampoEspecifico]
        public string Segmento1 { get; set; }

        [CampoArquivo("Site"), CampoEspecifico]
        public string Site { get; set; }

        [CampoArquivo("SITUACAOHOMINUM"), CampoEspecifico]
        public string Situacao { get; set; }

        [CampoArquivo("SUPERVISOR"), CampoEspecifico]
        public string Supervisor { get; set; }

        [CampoArquivo("UG"), CampoEspecifico]
        public string UG { get; set; }

        [CampoArquivo("externalID")]
        public int? IDExternalWebStation{ get; set; }

        [CampoArquivo("password")]
        public string Password { get; set; }

        [CampoArquivo("startDate"), DateFormat]
        public DateTime DataInicio { get; set; }

        [CampoArquivo("senDate"), DateFormat]
        public DateTime DataAntiga { get; set; }

        [CampoArquivo("senExt")]
        public string ExntensaoAntiga { get; set; }

        [CampoArquivo("rank")]
        public int? Rank { get; set; }

        [CampoArquivo("vacGroupID")]
        public int? VacationGroupID { get; set; }

        [CampoArquivo("vacGroupName")]
        public int? VacationGroupNome { get; set; }

        [CampoArquivo("biddingDate"), DateFormat]
        public DateTime? BiddingDate { get; set; }

        [CampoArquivo("biddingExt"), DateFormat]
        public DateTime? BiddingExit { get; set; }

        [CampoArquivo("accrualDate"), DateFormat]
        public DateTime? DataAcumulacao { get; set; }

        [CampoArquivo("email")]
        public string Email { get; set; }

        public void Teste() { int a = 10; }

    }
}
