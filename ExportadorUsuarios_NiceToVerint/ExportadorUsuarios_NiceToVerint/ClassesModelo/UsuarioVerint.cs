﻿using ExportadorUsuarios_NiceToVerint.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint.ClassesModelo
{
    public class UsuarioVerint
    {
        [CampoArquivo("custID")]
        public int clienteId { get; set; }

        [CampoCopiar("Nome")]
        public string Nome { get; set; }

        [CampoCopiar("Coordenador")]
        public string Coordenador { get; set; }

    }
}
