﻿using ExportadorUsuarios_NiceToVerint.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint.ClassesModelo
{
    public class ConversaoDadosVerint
    {
        public List<UsuarioVerint> NiceToVerint(List<UsuarioNice> usuariosNice)
        {
            List<UsuarioVerint> ret = new List<UsuarioVerint>();

            var propsVerint = typeof(UsuarioVerint).GetProperties
                (BindingFlags.Public | BindingFlags.Instance);
            propsVerint = propsVerint.Where(a => a.GetCustomAttribute(typeof(CampoCopiar)) != null).ToArray(); 

            foreach (UsuarioNice usuNice in usuariosNice)
            {
                UsuarioVerint usuVerint = new UsuarioVerint();

                foreach (var propVerint in propsVerint)
                {
                    var atributo = propVerint.GetCustomAttribute(typeof(CampoCopiar)) as CampoCopiar;
                    var propNice = typeof(UsuarioNice).GetProperty(atributo.Nome);
                    var valorNice = propNice.GetValue(usuNice);

                    propVerint.SetValue(usuVerint, valorNice);
                }

                ret.Add(usuVerint);
            }
            return ret;
        }
    }
}
