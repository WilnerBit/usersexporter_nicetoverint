﻿using ExportadorUsuarios_NiceToVerint.Atributos;
using ExportadorUsuarios_NiceToVerint.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint.ClassesModelo
{
    public class ConversaoDadosNice
    {
        protected List<DicionarioOrdenado<string, string>> DicionarioUsuariosNice;
        protected List<string> CamposLidos;
        public List<DicionarioOrdenado<string, string>> GetDictionaryUsuarioNice
            (List<string> linhasArquivo)
        {

            DicionarioUsuariosNice = new List<DicionarioOrdenado<string, string>>();

            var linhaNomeCampos = linhasArquivo[0];
            var linhaOrdemRegistros = linhasArquivo[1];

            linhasArquivo.Remove(linhaNomeCampos);
            linhasArquivo.Remove(linhaOrdemRegistros);

            linhaNomeCampos = linhaNomeCampos.Replace("#fields:", "");
            linhaOrdemRegistros = linhaOrdemRegistros.Replace("#sort:", "");

            CamposLidos = linhaNomeCampos.Split('|').ToList();


            foreach (string linha in linhasArquivo)
            {
                var camposUsuarioAtual = linha.Split('|');
                DicionarioOrdenado<string, string> novoUsuario = new DicionarioOrdenado<string, string>();

                for (int i = 0; i < CamposLidos.Count; i++)
                {
                    var nomeCampo = CamposLidos[i];
                    var valorCampo = camposUsuarioAtual[i];

                    novoUsuario.Add(nomeCampo, valorCampo);
                }

                DicionarioUsuariosNice.Add(novoUsuario);
            }

            return DicionarioUsuariosNice;
        }

        public List<UsuarioNice> ConvertDicionarioToUsers
            (List<DicionarioOrdenado<string, string>> dicionario)
        {
            List<UsuarioNice> ret = new List<UsuarioNice>();

            var propiedades = typeof(UsuarioNice).GetProperties
                (BindingFlags.Public | BindingFlags.Instance);
            foreach (DicionarioOrdenado<string, string> item in dicionario)
            {
                UsuarioNice novoUsuario = new UsuarioNice();

                foreach (var prop in propiedades)
                {
                    var atrib = prop.GetCustomAttribute(typeof(CampoArquivo)) as CampoArquivo;
                    var nomeOrigem = atrib.Nome;
                    var valorString = item[nomeOrigem];
                    GenericValueConverter.SetPropertyValueFromString
                        (novoUsuario, prop.Name, valorString);
                }

                ret.Add(novoUsuario);
            }

            return ret;
        }

    }

    public class GenericValueConverter
    {
        public static void SetPropertyValueFromString(object target, string propertyName, string value)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            // It throws Argument null exception  

            PropertyInfo oProp = target.GetType().GetProperty(propertyName);
            Type tProp = oProp.PropertyType;

            var formatDate = oProp.GetCustomAttribute(typeof(DateFormat)) as DateFormat;
            if (formatDate != null)
            {
                DateTime valor = default(DateTime);
                foreach (string formato in formatDate.Formatos)
                {
                    if (DateTime.TryParseExact(value, formato, provider, DateTimeStyles.None, out valor))
                        break;
                }

                var valorSetar = valor != default(DateTime) ? (DateTime?)valor : null;
                oProp.SetValue(target, valorSetar, null);
            }
            else if (oProp.PropertyType == typeof(Boolean))
            {
                var valorBoolean = !String.IsNullOrWhiteSpace(value) && value.Trim() == "1";
                oProp.SetValue(target, valorBoolean, null);
            }
            else
            {

                Type t = Nullable.GetUnderlyingType(oProp.PropertyType) ?? oProp.PropertyType;

                object safeValue = (String.IsNullOrWhiteSpace(value)) ? null : Convert.ChangeType(value, t);

                oProp.SetValue(target, safeValue, null);

            }
        }
    }


}
