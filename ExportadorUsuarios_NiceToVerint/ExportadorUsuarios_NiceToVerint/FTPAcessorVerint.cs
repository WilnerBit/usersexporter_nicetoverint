﻿using ExportadorUsuarios_NiceToVerint.ClassesModelo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExportadorUsuarios_NiceToVerint
{
    public class FTPAcessorVerint
    {
        public void MontaArquivoXMLSaida(List<UsuarioVerint> usuarios)
        {
            //UsuarioDestino usuario = new UsuarioDestino();
            //usuario.Id = 1;
            //usuario.Nome = "Wilner";
            //usuario.Endereco = "Rua Antonieta";
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer
                (typeof(List<UsuarioVerint>));


            var path = String.Format("{0}//Usuarios{1}.xml", 
                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), 
                DateTime.Now.ToString("ddMMyyyy_HHmmss"));
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, usuarios);
            file.Close();
            //x.Serialize(Console.Out, usuario);

            byte[] fileContents;
            using (StreamReader sourceStream = new StreamReader(path))
            {
                fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            }

            var request = GetRequestVerint(WebRequestMethods.Ftp.UploadFile, Path.GetFileName(path));
           // request.ContentLength = fileContents.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(fileContents, 0, fileContents.Length);
            }
        }

        protected FtpWebRequest GetRequestVerint(string method, string fileName)
        {
            var enderecoVerint = ConfigurationManager.AppSettings.Get("EnderecoFTPVerint");
            var usuarioVerint = ConfigurationManager.AppSettings.Get("UsuarioFTPVerint");
            var senhaVerint = ConfigurationManager.AppSettings.Get("SenhaFTPVerint");

            if (!String.IsNullOrWhiteSpace(fileName))
                enderecoVerint = String.Format(@"{0}/{1}", enderecoVerint, fileName);


            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(enderecoVerint);
            request.Method = method;
            //request.Method = WebRequestMethods.Ftp.ListDirectory;

            // This example assumes the FTP site uses anonymous logon.  
            request.Credentials = new NetworkCredential(usuarioVerint, senhaVerint);
            request.KeepAlive = false;
            request.UseBinary = true;
            request.UsePassive = true;

            return request;
        }
    }
}
