﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using ExportadorUsuarios_NiceToVerint.ClassesModelo;
using ExportadorUsuarios_NiceToVerint.Util;

namespace ExportadorUsuarios_NiceToVerint
{
    class Program
    {
        public static void Main(string[] args)
        {
            //EscondeConsole();
            ExecutaPrograma();

        }

        public static void EscondeConsole()
        {
            const int valorHide = 0;
            const int ValorShow = 5;

            var handle = GetConsoleWindow();
            ShowWindow(handle, valorHide);
        }

        public static void ExecutaPrograma()
        {
            FTPAcessorNice ftpNice = new FTPAcessorNice();
            var linhasArquivoNice = ftpNice.CarregaLinhasArquivoNice();

            ConversaoDadosNice conversorNice = new ConversaoDadosNice();
            var dicionario = conversorNice.GetDictionaryUsuarioNice(linhasArquivoNice);

            VisualizacaoExcel visualizador = new VisualizacaoExcel();
            visualizador.GeraExcelDados(dicionario);

            var usuariosNice = conversorNice.ConvertDicionarioToUsers(dicionario);

            ConversaoDadosVerint conversorVerint = new ConversaoDadosVerint();
            var usuariosVerint = conversorVerint.NiceToVerint(usuariosNice);

            FTPAcessorVerint ftpVerint = new FTPAcessorVerint();
            ftpVerint.MontaArquivoXMLSaida(usuariosVerint);

            Console.WriteLine();

            Console.ReadLine();
        }

        public static List<UsuarioDestino> GetUsuariosOutput()
        {
            List<UsuarioDestino> ret = new List<UsuarioDestino>();
            ret.Add(new UsuarioDestino() { Id = 1, Endereco = "Antonieta", Nome = "Wilner" });
            ret.Add(new UsuarioDestino() { Id = 2, Endereco = "Paulo Faccino", Nome = "Anderson" });
            ret.Add(new UsuarioDestino() { Id = 3, Endereco = "Ofion", Nome = "Felipe" });

            return ret;
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
    }
}
